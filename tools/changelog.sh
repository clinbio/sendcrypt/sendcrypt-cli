#!/bin/bash

printf '%s\n' "$(tput bold)$(tput smul)$(git -C "$SENDCRYPT" rev-parse --abbrev-ref HEAD)$(tput rmul)$(tput sgr0)"
echo ""
printf '%s\n' "$(tput bold)Changelog:$(tput sgr0)"
echo ""
git -C "$SENDCRYPT" log --pretty=format:"- %h [%s] %b" | while IFS= read -r line; do
  echo "$line"
done
echo ""