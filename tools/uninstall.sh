#!/bin/bash

read -r -p "Are you sure you want to remove SendCrypt? [y/N] " confirmation
if [ "$confirmation" != y ] && [ "$confirmation" != Y ]; then
  echo "Uninstall cancelled"
  exit
fi

echo "Removing ~/.sendcrypt"
if [ -d ~/.sendcrypt ]; then
  rm -rf ~/.sendcrypt
fi

echo "Thanks for using SendCrypt. It's been uninstalled."
echo "Don't forget to restart your terminal!"