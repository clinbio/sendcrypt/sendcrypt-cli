#!/bin/bash
#
# This script should be run via curl:
#  sh -c "$(curl -fsSL https://gitlab.sib.swiss/clinbio/sendcrypt/sendcrypt-cli/-/raw/main/tools/install.sh)"
# or wget:
#  sh -c "$(wget -qO- https://gitlab.sib.swiss/clinbio/sendcrypt/sendcrypt-cli/-/raw/main/tools/install.sh)"
# or via fetch:
#  sh -c "$(fetch -o - https://gitlab.sib.swiss/clinbio/sendcrypt/sendcrypt-cli/-/raw/main/tools/install.sh)"
#
# As an alternative, you can first download the install script and run it afterwards:
#  wget https://gitlab.sib.swiss/clinbio/sendcrypt/sendcrypt-cli/-/raw/main/tools/install.sh
#  sh install.sh
set -e

# Make sure important variables exist if not already defined
#
# $USER is defined by login(1) which is not always executed (e.g. containers)
# POSIX: https://pubs.opengroup.org/onlinepubs/009695299/utilities/id.html
USER=${USER:-$(id -u -n)}
# $HOME is defined at the time of login, but it could be unset. If it is unset,
# a tilde by itself (~) will not be expanded to the current user's home directory.
# POSIX: https://pubs.opengroup.org/onlinepubs/009696899/basedefs/xbd_chap08.html#tag_08_03
HOME="${HOME:-$(getent passwd $USER 2>/dev/null | cut -d: -f6)}"
# macOS does not have getent, but this works even if $HOME is unset
HOME="${HOME:-$(eval echo ~$USER)}"

# Default value for $SENDCRYPT
SENDCRYPT="${SENDCRYPT:-$HOME/.sendcrypt}"

# Default settings
REPO=${REPO:-clinbio/sendcrypt/sendcrypt-cli}
REMOTE=${REMOTE:-https://gitlab.sib.swiss/${REPO}.git}
BRANCH=${BRANCH:-main}

if [ -z "$TERM" ] && which tput >/dev/null 2>&1; then
  FMT_BLUE=$(tput setaf 4)
  FMT_GREEN=$(tput setaf 2)
  FMT_RED=$(tput setaf 1)
  FMT_RESET=$(tput sgr0)
fi

has_git() {
  command -v "git" >/dev/null 2>&1
}

setup_sendcrypt() {
  # Prevent the cloned repository from having insecure permissions. Failing to do
  # so causes compinit() calls to fail with "command not found: compdef" errors
  # for users with insecure umasks (e.g., "002", allowing group writability). Note
  # that this will be ignored under Cygwin by default, as Windows ACLs take
  # precedence over umasks except for filesystems mounted with option "noacl".
  umask g-w,o-w

  echo "${FMT_BLUE}Cloning SendCrypt...${FMT_RESET}"

  has_git || {
    echo "${FMT_RED}Error: git is not installed${FMT_RESET}"
    exit 1
  }

  ostype=$(uname)
  if [[ "$ostype" == CYGWIN* || "$ostype" == MINGW* || "$ostype" == MSYS* ]]; then
    echo "${FMT_RED}Error: Windows is not supported${FMT_RESET}"
    exit 1
  fi

  # Manual clone with git config options to support git < v1.7.2
  git init --quiet "$SENDCRYPT" && cd "$SENDCRYPT" \
  && git config core.eol lf \
  && git config core.autocrlf false \
  && git config fsck.zeroPaddedFilemode ignore \
  && git config fetch.fsck.zeroPaddedFilemode ignore \
  && git config receive.fsck.zeroPaddedFilemode ignore \
  && git config sendcrypt-cli.remote origin \
  && git config sendcrypt-cli.branch "$BRANCH" \
  && git remote add origin "$REMOTE" \
  && git fetch --depth=1 origin \
  && git checkout -b "$BRANCH" "origin/$BRANCH" || {
    [ ! -d "$SENDCRYPT" ] || {
      cd -
      rm -rf "$SENDCRYPT" 2>/dev/null
    }
    echo "${FMT_RED}Error: git clone of sendcrypt repo failed${FMT_RESET}"
    exit 1
  }
  # Exit installation directory
  cd -

  echo
}

write_default_profile() {
  printf 'SENDCRYPT_SFTP_USER=
SENDCRYPT_SFTP_HOST=
SENDCRYPT_SFTP_PORT=
SENDCRYPT_SFTP_REMOTE_PATH=
SENDCRYPT_SSH_KEY=
SENDCRYPT_GPG_SENDER=
SENDCRYPT_GPG_RECIPIENT=
SENDCRYPT_GPG_PASSPHRASE=
SENDCRYPT_PROJECT=' > "$SENDCRYPT/profiles/default.env"
}

print_success() {
  echo "${FMT_GREEN}SendCrypt is now installed and ready to use!${FMT_RESET}"
  echo
  echo "To get started, please run:"
  echo
  echo "  sendcrypt --help"
  echo
  echo "or visit https://gitlab.sib.swiss/clinbio/sendcrypt/sendcrypt-cli for more information."
  echo
}

main() {
  setup_sendcrypt
  write_default_profile
  print_success
}

main