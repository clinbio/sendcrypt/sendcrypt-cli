#!/bin/bash

source "$SENDCRYPT/lib/logger.sh"

return_code=0

cd "$SENDCRYPT"

# Set git-config values known to fix git errors
# Line endings (#4069)
git config core.eol lf
git config core.autocrlf false
# zeroPaddedFilemode fsck errors (#4963)
git config fsck.zeroPaddedFilemode ignore
git config fetch.fsck.zeroPaddedFilemode ignore
git config receive.fsck.zeroPaddedFilemode ignore
# autostash on rebase (#7172)
resetAutoStash=$(git config --bool rebase.autoStash 2>/dev/null)
git config rebase.autoStash true

# repository settings
remote=$(git config --local sendcrypt-cli.remote) || echo "origin"
branch=$(git config --local sendcrypt-cli.branch) || echo "main"

# repository state
last_head=$(git symbolic-ref --quiet --short HEAD || git rev-parse HEAD)
# checkout update branch
git checkout -q "$branch" -- || exit 1
# branch commit before update (used in changelog)
last_commit=$(git rev-parse "$branch")

sendcrypt_log "info" "Updating SendCrypt"

if LANG= git pull --quiet --rebase $remote $branch; then
  # Check if it was really updated or not
  if [[ "$(git rev-parse HEAD)" = "$last_commit" ]]; then
    message="SendCrypt is already at the latest version."
  else
    message="Hooray! SendCrypt has been updated!"

    # Save the commit prior to updating
    git config sendcrypt-cli.lastVersion "$last_commit"

    # Print changelog to the terminal
    "$SENDCRYPT/tools/changelog.sh"
  fi

  sendcrypt_log "success" "$message"
else
  return_code=$?
  sendcrypt_log "error" "There was an error updating SendCrypt. Try again later."
fi

# go back to HEAD previous to update
git checkout -q "$last_head" --

# Unset git-config values set just for the upgrade
case "$resetAutoStash" in
  "") git config --unset rebase.autoStash ;;
  *) git config rebase.autoStash "$resetAutoStash" ;;
esac

# Exit with `1` if the update failed
exit $return_code