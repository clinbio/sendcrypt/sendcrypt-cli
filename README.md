# SendCrypt CLI

SendCrypt is an open source CLI developed by SIB that allows you to send encrypted files in a similar way to [SETT](https://gitlab.com/biomedit/sett).

To learn more, check [the documentation](https://clinbiokb.sib.swiss/s/sendcrypt/).

_SendCrypt has a Desktop application that allows you to send files in a more user-friendly way. You can find it [here](https://github.com/sib-swiss/sendcrypt)._

## Table of contents

- [Getting started](#getting-started)
  - [Pre-requisites](#pre-requisites)
  - [Installation](#installation)
- [Using SendCrypt](#using-sendcrypt)
  - [Sending files](#sending-files)
  - [Preparing a directory (for manual sending)](#preparing-a-directory-for-manual-sending)
- [Documentation](#documentation)
- [Running diagnostics](#running-diagnostics)
- [Getting updates](#getting-updates)
- [Uninstalling SendCrypt](#uninstalling-sendcrypt)
- [Contributing](#contributing)
- [License](#license)

## Getting started

### Pre-requisites

- [Bash](https://www.gnu.org/software/bash/)
- [Git](https://git-scm.com/)
- [GnuPG](https://gnupg.org/)
- [SSH](https://www.openssh.com/)
- [curl](https://curl.se/)


#### MacOS specific pre-requisites

- [gtar](https://www.gnu.org/software/tar/)
- [shasum](https://ss64.com/osx/shasum.html)

#### Linux specific pre-requisites

- [tar](https://www.gnu.org/software/tar/)
- [sha256sum](https://www.gnu.org/software/coreutils/manual/html_node/sha2-utilities.html)

### Installation

SendCrypt is installed by running one of the following commands in your terminal. You can install this via the command-line with either `curl`, `wget` or another similar tool.

| Method    | Command                                                                                                      |
|:----------|:-------------------------------------------------------------------------------------------------------------|
| **curl**  | `sh -c "$(curl -fsSL https://gitlab.sib.swiss/clinbio/sendcrypt/sendcrypt-cli/-/raw/main/tools/install.sh)"` |
| **wget**  | `sh -c "$(wget -qO- https://gitlab.sib.swiss/clinbio/sendcrypt/sendcrypt-cli/-/raw/main/tools/install.sh)"`  |
| **fetch** | `sh -c "$(fetch -o - https://gitlab.sib.swiss/clinbio/sendcrypt/sendcrypt-cli/-/raw/main/tools/install.sh)"` |

To make the `sendcrypt` command available in your terminal, you need to add the following line to your `~/.bashrc` file:

```bash
export PATH="$HOME/.sendcrypt:$PATH"
```

If you are using `zsh`, you need to add the following line to your `~/.zshrc` file:

```bash
export PATH="$HOME/.sendcrypt:$PATH"
```

Then, run the following command:

```bash
source ~/.bashrc
```

or

```bash
source ~/.zshrc
```

## Using SendCrypt

### Sending a directory

To send files, run the following command:

```bash
sendcrypt send <path-to-directory>
```

If you would like to specify a different profile, use the `-p` or `--profile` option:

```bash
sendcrypt -p <profile-name> send <path-to-directory>
```

### Preparing a directory (for manual sending)

This command allows you to prepare a directory according to the SendCrypt format but nothing will be sent. This covers a scenario where you would like to prepare a directory and send it manually.

To prepare a directory, run the following command:

```bash
sendcrypt prepare <path-to-directory>
```

If you would like to specify a different profile, use the `-p` or `--profile` option:

```bash
sendcrypt -p <profile-name> prepare <path-to-directory>
```

*Note that the directory will be created where you run the command.*

## Documentation

You can find the documentation for SendCrypt CLI [here](https://clinbiokb.sib.swiss/s/sendcrypt/doc/sendcrypt-cli-MNIRTX3KqL).

## Running diagnostics

To run diagnostics, run the following command:

```bash
sendcrypt doctor
```

This will check the following:

- Required libraries are installed
- Required commands are available
- Internet connection is available
- Connection to the SFTP server is possible
- Profile files are correctly encoded

_If you are only authorizing external connections to a SFTP server and nothing else, the check of the internet connection will fail. This is normal._

## Getting updates

To get updates, run the following command:

```bash
sendcrypt update
```

## Uninstalling SendCrypt

If you would like to uninstall SendCrypt, run the following command:

```bash
bash ~/.sendcrypt/tools/uninstall.sh
```

It will remove itself.

## Contributing

If you would like to contribute to SendCrypt, please read our [contributing guidelines](CONTRIBUTING.md).

## License

SendCrypt is open source software licensed under the [MIT license](LICENSE.md).

