sendcrypt_log() {
    level="$1"
    message="$2"
    timestamp=$(date +"%Y-%m-%d %H:%M:%S")

    # Create log entry
    log_entry="[$timestamp][$level] $message"

    # Save to log file
    echo "$log_entry" >> "$SENDCRYPT_LOG/sendcrypt.log"

    # Display message in the terminal with appropriate prompt
    case "$level" in
        "success")
            echo -e "\033[32m[SUCCESS]\033[0m $message"
            ;;
        "info")
            echo -e "\033[34m[INFO]\033[0m $message"
            ;;
        "warn")
            echo -e "\033[33m[WARNING]\033[0m $message"
            ;;
        "error")
            echo -e "\033[31m[ERROR]\033[0m $message"
            ;;
        *)
            echo "$log_entry"
            ;;
    esac
}