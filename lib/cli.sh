#!/bin/bash

source "$SENDCRYPT/lib/logger.sh"

sendcrypt_help() {
    cat <<EOF
Usage: sendcrypt [options] <command>

Available commands:

  help                Print this help message
  changelog           Print the changelog
  doctor              Check the system for any issues
  login               Log in to the sendcrypt server
  logout              Log out from the sendcrypt server
  profile             Manage and create profiles
  prepare <command>   Compress, encrypt, generate metadata and sign metadata
  send  <command>     Compress, encrypt, generate metadata, sign metadata and send by SFTP
  update              Update sendcrypt-cli
  version             Print the version number

EOF
}

sendcrypt_prepare_help() {
    cat <<EOF
Usage: sendcrypt [options] prepare <directory>

Available options:

  -p, --profile <profile>   Use a specific profile (default: default)

EOF
}

sendcrypt_send_help() {
    cat <<EOF
Usage: sendcrypt [options] send <directory>

Available options:

  -p, --profile <profile>   Use a specific profile (default: default)

EOF
}

sendcrypt_version() {
    echo "$SENDCRYPT_VERSION"
}

sendcrypt_check_access_to_server() {
    if ! sftp -q -P "$SENDCRYPT_SFTP_PORT" -i "$SENDCRYPT_SSH_KEY" "$SENDCRYPT_SFTP_USER"@"$SENDCRYPT_SFTP_HOST" <<< "exit" &> /dev/null; then
        sendcrypt_log "error" "Unable to connect to the remote server (SFTP)."
        exit 1
    else
        sendcrypt_log "success" "Access to the remote server is OK (SFTP)."
    fi
}

sendcrypt_check_internet_connection() {
    if ! curl -s -I https://sendcrypt.sib.swiss > /dev/null; then
        sendcrypt_log "error" "Unable to reach sendcrypt.sib.swiss (HTTPS)."
        exit 1
    else
        sendcrypt_log "success" "sendcrypt.sib.swiss is reachable (HTTPS)."
    fi
}

sendcrypt_check_dependencies() {
     os_name="$(uname -s)"

      case "$os_name" in
        Linux)
          tar_command="tar"
          checksum_command="sha256sum"
          ;;
        Darwin)
          tar_command="gtar"
          checksum_command="shasum"
          ;;
        *)
          sendcrypt_log "error" "This script is not supported on Windows."
          exit 1
          ;;
      esac

    if ! command -v "$tar_command" > /dev/null; then
        sendcrypt_log "error" "The command '$tar_command' is not installed."
        exit 1
    fi

    if ! command -v "$checksum_command" > /dev/null; then
        sendcrypt_log "error" "The command '$checksum_command' is not installed."
        exit 1
    fi

    if ! command -v gpg > /dev/null; then
        sendcrypt_log "error" "The command 'gpg' is not installed."
        exit 1
    fi

    if ! command -v zip > /dev/null; then
        sendcrypt_log "error" "The command 'zip' is not installed."
        exit 1
    fi

    if ! command -v ssh > /dev/null; then
        sendcrypt_log "error" "The command 'ssh' is not installed."
        exit 1
    fi

    if ! command -v sftp > /dev/null; then
        sendcrypt_log "error" "The command 'sftp' is not installed."
        exit 1
    fi

    sendcrypt_log "success" "All dependencies are installed."
}

sendcrypt_check_profiles() {
    for profile in "$SENDCRYPT_PROFILES"/*; do
        encoding=$(file -b --mime-encoding "$profile")
        if [[ "$encoding" != "us-ascii" ]]; then
            sendcrypt_log "error" "The profile '$profile' is not encoded in ASCII. It's encoded as '$encoding'."
            exit 1
        fi
    done

    sendcrypt_log "success" "All profiles are encoded in ASCII."
}

sendcrypt_doctor() {
    sendcrypt_check_dependencies
    sendcrypt_check_profiles
    sendcrypt_check_access_to_server
    sendcrypt_check_internet_connection

    sendcrypt_log "success" "Everything looks good!"
}