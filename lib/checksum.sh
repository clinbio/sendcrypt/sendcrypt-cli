#!/bin/bash

source "$SENDCRYPT/lib/logger.sh"

sendcrypt_checksums() {
    os_name="$(uname -s)"

    case "$os_name" in
      Linux)
        checksum_command="sha256sum"
        ;;
      Darwin)
        checksum_command="shasum -a 256"
        ;;
      *)
        sendcrypt_log "error" "This script is not supported on Windows."
        exit 1
        ;;
    esac

    if [ -z "$2" ]; then
      checksum_file="checksum.sha256"
    else
      checksum_file="$2"
    fi

    # Get the directory from the path
    path=$1

    echo "$path"

    if [ ! -d "$1" ]; then
        sendcrypt_log "error" "Folder '$1' does not exist."
        exit 1
    fi

    find "$1" -type f -exec $checksum_command "{}" + | sed "s|$path/||" > "$checksum_file"

    sendcrypt_log "info" "Created checksum file '$checksum_file'"
}