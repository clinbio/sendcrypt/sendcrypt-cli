#!/bin/bash

source "$SENDCRYPT/lib/logger.sh"

sendcrypt_compress_to_tar() {
    os_name="$(uname -s)"

    case "$os_name" in
      Linux)
        tar_command="tar"
        ;;
      Darwin)
        tar_command="gtar"
        ;;
      *)
        sendcrypt_log "error" "This script is not supported on Windows."
        exit 1
        ;;
    esac

    if [ "$#" -eq 0 ]; then
        sendcrypt_log "error" "No folder specified."
        exit 1
    fi

    if [ ! -d "$1" ]; then
        sendcrypt_log "error" "Folder '$1' does not exist."
        exit 1
    fi

    if [ -z "$2" ]; then
        archive_file="data.tar"
    else
        archive_file="$2"
    fi

    if ! $tar_command -cf "$archive_file" --transform "s|^|content/|" -C "$1" . > /dev/null 2>&1; then
        sendcrypt_log "error" "Unable to create archive file '$archive_file'"
        exit 1
    fi

    sendcrypt_log "info" "Created archive file '$archive_file'"
}

sendcrypt_add_checksums_to_tar() {
  os_name="$(uname -s)"

      case "$os_name" in
        Linux)
          tar_command="tar"
          ;;
        Darwin)
          tar_command="gtar"
          ;;
        *)
          sendcrypt_log "error" "This script is not supported on Windows."
          exit 1
          ;;
      esac

      if [ "$#" -eq 0 ]; then
          sendcrypt_log "error" "No folder specified."
          exit 1
      fi

      if [ ! -d "$1" ]; then
          sendcrypt_log "error" "Folder '$1' does not exist."
          exit 1
      fi

      if [ -z "$2" ]; then
          archive_file="data.tar.gz"
      else
          archive_file="$2"
      fi

      cd "$1" || exit 1

      if ! $tar_command --append --file="$archive_file" "checksum.sha256" > /dev/null 2>&1; then
          sendcrypt_log "error" "Unable to add file '$1' to archive '$archive_file'"
          exit 1
      fi

      cd - > /dev/null 2>&1 || exit 1

      sendcrypt_log "info" "Added file '$1' to archive '$archive_file'"
}

sendcrypt_compress_to_gzip() {
  if ! gzip -qf "$1" > "$2" 1>/dev/null 2>&1; then
    sendcrypt_log "error" "Unable to create archive file '$2'"
    exit 1
  fi

  sendcrypt_log "info" "Created archive file '$2'"
}

sendcrypt_compress_to_zip() {
  if ! zip -0 -q -j -r "$1" "$@" 1>/dev/null 2>&1; then
    sendcrypt_log "error" "Unable to create archive file '$1'"
    exit 1
  fi

  sendcrypt_log "info" "Created archive file '$1'"
}
