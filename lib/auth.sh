#!/bin/bash

source "$SENDCRYPT/lib/logger.sh"

sendcrypt_login() {
  url="https://sendcrypt.sib.swiss/api/v1/login"

  read -rp "Enter email: " email
  read -rsp "Enter password: " password
  device_name='sendcrypt-cli'
  echo

  temp_file=$(mktemp "${TMPDIR:-/tmp}/sendcrypt.XXXXXX")

  response=$(curl -s -o "$temp_file" -w "%{http_code}" -X POST "$url" -H "Accept: application/json" -H "Content-Type: application/json" -d "{\"email\":\"$email\", \"password\":\"$password\", \"device_name\":\"$device_name\"}")
  if [ "$response" -eq 200 ]; then
      token=$(grep -o '"token":"[^"]*' "$temp_file" | sed 's/"token":"//')
      sendcrypt_log "success" "Login successful."
      echo "{\"email\":\"$email\", \"token\":\"$token\"}" > "$SENDCRYPT/config.json"
  else
      message=$(grep -o '"message":"[^"]*' "$temp_file" | sed 's/"message":"//')
      sendcrypt_log "error" "Failed to log in. Status code: $response : $message"
  fi

  rm "$temp_file" 2> /dev/null
}

sendcrypt_logout() {
  url="https://sendcrypt.sib.swiss/api/v1/logout"

  if [ -f "$SENDCRYPT/config.json" ]; then
    token=$(grep -o '"token":"[^"]*' "$SENDCRYPT/config.json" | sed 's/"token":"//')
  fi
  curl -s -o /dev/null -X POST "$url" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $token"
  rm "$SENDCRYPT/config.json" 2> /dev/null
  sendcrypt_log "info" "Logout successful."
}

sendcrypt_token() {
  if [ -f "$SENDCRYPT/config.json" ]; then
    token=$(grep -o '"token":"[^"]*' "$SENDCRYPT/config.json" | sed 's/"token":"//')
    echo "$token"
  fi

  echo ''
}