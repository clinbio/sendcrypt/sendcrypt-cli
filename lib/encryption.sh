#!/bin/bash

source "$SENDCRYPT/lib/logger.sh"

sendcrypt_encrypt() {
    recipient="$1"
    file="$2"
    encrypted_file="${file}.gpg"

    if [ ! -f "$file" ]; then
        sendcrypt_log "error" "File '$file' does not exist."
        exit 1
    fi

    if ! gpg --quiet --batch --yes --encrypt --trust-model always --recipient "$recipient" --output "$encrypted_file" "$file" 2>/dev/null; then
        sendcrypt_log "error" "Unable to encrypt file '$file'"
        exit 1
    fi

    sendcrypt_log "info" "Created encrypted file '$encrypted_file'"
}

sendcrypt_sign() {
    if [ ! -f "$1" ]; then
        sendcrypt_log "error" "File '$1' does not exist."
        exit 1
    fi

    if [ -z "$2" ]; then
      signed_file="${1}.sig"
    else
      signed_file="$2"
    fi

    if [ -z "$SENDCRYPT_GPG_PASSPHRASE" ]; then
      if ! gpg --batch --yes --local-user "$SENDCRYPT_GPG_SENDER" --no --detach-sign --output "$signed_file" "$1" 2>/dev/null; then
        sendcrypt_log "error" "Unable to sign file '$1'"
        exit 1
      fi
    else
      if ! gpg --batch --yes --pinentry-mode loopback --passphrase "$SENDCRYPT_GPG_PASSPHRASE" --local-user "$SENDCRYPT_GPG_SENDER" --detach-sign --output "$signed_file" "$1" 2>/dev/null; then
        sendcrypt_log "error" "Unable to sign file '$1'"
        exit 1
      fi
    fi

    sendcrypt_log "info" "Created signed file '$signed_file'"
}