#!/bin/bash

source "$SENDCRYPT/lib/logger.sh"

read_required() {
  prompt="$1"
  local user_input

  read -r -p "$prompt: " user_input

  if [ -z "$user_input" ]; then
    read_required "$prompt"
  else
    echo "$user_input"
  fi
}

read_optional() {
  prompt="$1"
  message="$2"
  local user_input

  read -r -p "$prompt $message:" user_input

  echo "$user_input"
}

read_sensitive() {
  prompt="$1"
  local user_input

  read -r -s -p "$prompt: " user_input

  echo "$user_input"
}

read_optional_sensitive() {
  prompt="$1"
  message="$2"
  local user_input

  read -r -s -p "$prompt $message: " user_input

  echo "$user_input"
}

read_with_default() {
  prompt="$1"
  default_value="$2"
  local user_input

  read -r -p "$prompt (press enter for default) [default: $default_value]: " user_input

  if [ -z "$user_input" ]; then
    echo "$default_value"
  else
    echo "$user_input"
  fi
}

sendcrypt_profile_create()  {
  SENDCRYPT_SFTP_USER=$(read_required "SENDCRYPT_SFTP_USER")
  SENDCRYPT_SFTP_HOST=$(read_required "SENDCRYPT_SFTP_HOST")
  SENDCRYPT_SFTP_PORT=$(read_with_default "SENDCRYPT_SFTP_PORT" "22")
  SENDCRYPT_SFTP_REMOTE_PATH=$(read_required "SENDCRYPT_SFTP_REMOTE_PATH")
  SENDCRYPT_SSH_KEY=$(read_with_default "SENDCRYPT_SSH_KEY" "$HOME/.ssh/id_rsa")
  SENDCRYPT_GPG_SENDER=$(read_required "SENDCRYPT_GPG_SENDER")
  SENDCRYPT_GPG_RECIPIENT=$(read_required "SENDCRYPT_GPG_RECIPIENT")
  SENDCRYPT_GPG_PASSPHRASE=$(read_optional_sensitive "SENDCRYPT_GPG_PASSPHRASE" "(leave blank for no passphrase)")
  echo ""
  SENDCRYPT_PROJECT=$(read_required "SENDCRYPT_PROJECT")
  SENDCRYPT_API_URL=$(read_optional "SENDCRYPT_API_URL" "(leave blank for no notification)")

  profile_name=""

  while true; do
    read -r -p "Profile Name (lowercase a-z and numbers only): " profile_name

    if [ -e "$SENDCRYPT_PROFILES/$profile_name" ]; then
      echo "A file with the same name already exists. Please choose a different name."
    elif [[ ! "$profile_name" =~ ^[a-z0-9]+$ ]]; then
      echo "Invalid profile name. Please use lowercase letters and numbers only."
    else
      break
    fi
  done

  profile_file="$SENDCRYPT_PROFILES/${profile_name}.env"
  {
    echo "SENDCRYPT_SFTP_USER=$SENDCRYPT_SFTP_USER"
    echo "SENDCRYPT_SFTP_HOST=$SENDCRYPT_SFTP_HOST"
    echo "SENDCRYPT_SFTP_PORT=$SENDCRYPT_SFTP_PORT"
    echo "SENDCRYPT_SFTP_REMOTE_PATH=$SENDCRYPT_SFTP_REMOTE_PATH"
    echo "SENDCRYPT_SSH_KEY=$SENDCRYPT_SSH_KEY"
    echo "SENDCRYPT_GPG_SENDER=$SENDCRYPT_GPG_SENDER"
    echo "SENDCRYPT_GPG_RECIPIENT=$SENDCRYPT_GPG_RECIPIENT"
    echo "SENDCRYPT_GPG_PASSPHRASE=\"$SENDCRYPT_GPG_PASSPHRASE\""
    echo "SENDCRYPT_PROJECT=\"$SENDCRYPT_PROJECT\""
    echo "SENDCRYPT_API_URL=\"$SENDCRYPT_API_URL\""
  } > "$profile_file"

  sendcrypt_log "info" "Profile '$profile_name' created in '$profile_file'"
}

sendcrypt_profile_list() {
  for profile in "$SENDCRYPT_PROFILES"/*; do
    profile_name=$(basename "$profile" .env)
    echo "${profile_name%.*}"
  done
}

sendcrypt_profile_help() {
  echo "Usage: sendcrypt profile <create|list>"
}