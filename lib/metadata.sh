#!/bin/bash

source "$SENDCRYPT/lib/logger.sh"

sendcrypt_get_fingerprint() {
  gpg --with-colons --fingerprint "$1" | awk -F: '$1 == "fpr" { gsub(" ", "", $10); print $10 }' | head -n 1
}

sendcrypt_checksum() {
  os_name="$(uname -s)"

      case "$os_name" in
        Linux)
          checksum_command="sha256sum"
          ;;
        Darwin)
          checksum_command="shasum -a 256"
          ;;
        *)
          sendcrypt_log "error" "This script is not supported on Windows."
          exit 1
          ;;
      esac

      $checksum_command "$1" | awk '{print $1}'
}

sendcrypt_generate_json() {
      if [ ! -f "$1" ]; then
          sendcrypt_log "error" "File '$1' does not exist."
          exit 1
      fi

      project=$SENDCRYPT_PROJECT
      sender=$(sendcrypt_get_fingerprint "$SENDCRYPT_GPG_SENDER")
      recipients=$(sendcrypt_get_fingerprint "$SENDCRYPT_GPG_RECIPIENT")
      timestamp=$(date +"%Y-%m-%dT%H:%M:%S%z")
      checksum=$(sendcrypt_checksum "$1")
      checksum_algorithm="SHA256"
      compression_algorithm="gzip"
      purpose="PRODUCTION"
      version="$SENDCRYPT_VERSION"

      printf '{
    "project": "%s",
    "sender": "%s",
    "recipients": ["%s"],
    "timestamp": "%s",
    "checksum": "%s",
    "checksum_algorithm": "%s",
    "compression_algorithm": "%s",
    "purpose": "%s",
    "version": "%s - CLI"
}\n' "$project" "$sender" "$recipients" "$timestamp" "$checksum" "$checksum_algorithm" "$compression_algorithm" "$purpose" "$version"
}

sendcrypt_metadata() {
    if ! sendcrypt_generate_json "$1" > "$2"; then
        sendcrypt_log "error" "Unable to create metadata file '$2'"
        exit 1
    fi
    sendcrypt_log "info" "Created metadata file '$2'"
}