#!/bin/bash

source "$SENDCRYPT/lib/logger.sh"

sendcrypt_sftp() {
    url="${SENDCRYPT_SFTP_USER}@${SENDCRYPT_SFTP_HOST}"
    url_with_path="$url:$SENDCRYPT_SFTP_REMOTE_PATH"
    file_name=$(basename "$1")
    temp_name="$SENDCRYPT_SFTP_REMOTE_PATH/$file_name.part"
    target_name="$SENDCRYPT_SFTP_REMOTE_PATH/$file_name"

    if ! echo -e "put \"$1\" \"$temp_name\"\nrename \"$temp_name\" \"$target_name\"" | \
     sftp -b- -P "$SENDCRYPT_SFTP_PORT" -i "$SENDCRYPT_SSH_KEY" "$url_with_path"; then
        sendcrypt_log "error" "Unable to send file '$1' to '$url'"
        exit 1
    fi

    sendcrypt_log "info" "Sent file '$1' to '$url'"
}