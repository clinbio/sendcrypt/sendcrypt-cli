#!/bin/bash

source "$SENDCRYPT/lib/logger.sh"
source "$SENDCRYPT/lib/auth.sh"

sendcrypt_notify() {
  url=$1
  metadata=$(cat "$2")

  temp_file=$(mktemp "${TMPDIR:-/tmp}/sendcrypt.XXXXXX")
  token=$(sendcrypt_token)

  response=$(curl -s -o "$temp_file" -w '%{http_code}' -X POST "$url" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $token" -d "$metadata")

  if [ "$response" -eq 200 ]; then
      sendcrypt_log "info" "Notification sent successfully."
  else
      message=$(grep -o '"message":"[^"]*' "$temp_file" | sed 's/"message":"//')
      sendcrypt_log "error" "Request failed. Status code: $response : $message"
      exit 1
  fi

  rm "$temp_file" 2> /dev/null
}